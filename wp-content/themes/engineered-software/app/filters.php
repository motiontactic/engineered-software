<?php

namespace App;

/**
 * Add <body> classes
 */
add_filter( 'body_class', function ( array $classes ) {
	/** Add page slug if it doesn't exist */
	if ( is_single() || is_page() && !is_front_page() ) {
		if ( !in_array( basename( get_permalink() ), $classes ) ) {
			$classes[] = basename( get_permalink() );
		}
	}

	/** Add class if sidebar is active */
	if ( display_sidebar() ) {
		$classes[] = 'sidebar-primary';
	}

	/** Clean up class names for custom templates */
	$classes = array_map( function ( $class ) {
		return preg_replace( [ '/-blade(-php)?$/', '/^page-template-views/' ], '', $class );
	}, $classes );

	return array_filter( $classes );
} );

/**
 * Add "… Continued" to the excerpt
 */
add_filter( 'excerpt_more', function () {
	return ' &hellip; <a href="' . get_permalink() . '">' . __( 'Continued', 'sage' ) . '</a>';
} );

/**
 * Template Hierarchy should search for .blade.php files
 */
collect( [
	'index', '404', 'archive', 'author', 'category', 'tag', 'taxonomy', 'date', 'home',
	'frontpage', 'page', 'paged', 'search', 'single', 'singular', 'attachment', 'embed'
] )->map( function ( $type ) {
	add_filter( "{$type}_template_hierarchy", __NAMESPACE__ . '\\filter_templates' );
} );

/**
 * Render page using Blade
 */
add_filter( 'template_include', function ( $template ) {
	collect( [ 'get_header', 'wp_head' ] )->each( function ( $tag ) {
		ob_start();
		do_action( $tag );
		$output = ob_get_clean();
		remove_all_actions( $tag );
		add_action( $tag, function () use ( $output ) {
			echo $output;
		} );
	} );
	$data = collect( get_body_class() )->reduce( function ( $data, $class ) use ( $template ) {
		return apply_filters( "sage/template/{$class}/data", $data, $template );
	}, [] );
	if ( $template ) {
		echo template( $template, $data );
		return get_stylesheet_directory() . '/index.php';
	}
	return $template;
}, PHP_INT_MAX );

/**
 * Render comments.blade.php
 */
add_filter( 'comments_template', function ( $comments_template ) {
	$comments_template = str_replace(
		[ get_stylesheet_directory(), get_template_directory() ],
		'',
		$comments_template
	);

	$data = collect( get_body_class() )->reduce( function ( $data, $class ) use ( $comments_template ) {
		return apply_filters( "sage/template/{$class}/data", $data, $comments_template );
	}, [] );

	$theme_template = locate_template( [ "views/{$comments_template}", $comments_template ] );

	if ( $theme_template ) {
		echo template( $theme_template, $data );
		return get_stylesheet_directory() . '/index.php';
	}

	return $comments_template;
}, 100 );

//Post Meta
add_filter( 'wp_head', function () {

	global $post;
	$post_type = get_post_type();
	$meta = '';

	if ( $post_type == 'post' || $post_type == 'product' ) {

		// Collecting the values
		$description = str_replace( '"', "'", strip_tags( \App::excerpt( $post->ID ) ) );
		$title = get_the_title();

		$meta .= "<meta property='og:title' content='$title' />\n";

		$share_image_url = env( 'WP_HOME' ) . get_field( 'social_share_image', "OPTIONS" );

		// Conditionally adding the tags
		if ( $description ) {

			$meta .= "<meta property='og:description' content='" . $description . "' />\n";
			$meta .= "<meta property='twitter:description' content='" . $description . "' />\n";
		}

		if ( $share_image_url ) $meta .= "<meta property='og:image' content='$share_image_url' />\n";

		echo $meta;
	}

} );

//favicon
add_filter( 'wp_head', function () {

	$meta = '';

	$meta .= "<link rel='apple-touch-icon' sizes='76x76' href='" . asset_path( 'images/meta/apple-touch-icon.png' ) . "'>\n";
	$meta .= "<link rel='icon' type='image/png' sizes='32x32' href='" . asset_path( 'images/meta/favicon-32x32.png' ) . "'>\n";
	$meta .= "<link rel='icon' type='image/png' sizes='16x16' href='" . asset_path( 'images/meta/favicon-16x16.png' ) . "'>\n";
	$meta .= "<link rel='manifest' href='" . asset_path( 'images/meta/site.webmanifest' ) . "'>\n";
	$meta .= "<link rel='mask-icon' href='" . asset_path( 'images/meta/safari-pinned-tab.svg' ) . "' color='#5bbad5'>\n";
	$meta .= "<meta name='msapplication-TileColor' content='#da532c'>\n";
	$meta .= "<meta name='theme-color' content='#ffffff'>\n";

	echo $meta;

} );

//redirect back to sign in page when error state
add_action( 'wp_login_failed', function ( $username ) {
	$referrer = $_SERVER[ 'HTTP_REFERER' ];  // where did the post submission come from?
	// if there's a valid referrer, and it's not the default log-in screen
	if ( !empty( $referrer ) && !strstr( $referrer, 'wp-login' ) && !strstr( $referrer, 'wp-admin' ) ) {
		if ( !strstr( $referrer, 'login=failed' ) ) {
			wp_redirect( $referrer . '?login=failed' ); // let's append some information (login=failed) to the URL for the theme to use
		} else {
			wp_redirect( $referrer );
		}
		exit;
	}
} );

//redirect back to the sign-in page once you logout
add_action( 'wp_logout', function () {
	wp_redirect( site_url( '/sign-in' ) );
	exit();
} );

add_filter( 'wp_head', function () {

	echo get_field( 'bug_herd', 'OPTIONS' );

} );


