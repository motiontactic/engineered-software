<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
	public function siteName()
	{
		return get_bloginfo( 'name' );
	}

	public static function title()
	{
		if ( is_home() ) {
			if ( $home = get_option( 'page_for_posts', true ) ) {
				return get_the_title( $home );
			}
			return __( 'Latest Posts', 'sage' );
		}
		if ( is_archive() ) {
			return get_the_archive_title();
		}
		if ( is_search() ) {
			return sprintf( __( 'Search Results for %s', 'sage' ), get_search_query() );
		}
		if ( is_404() ) {
			return __( 'Not Found', 'sage' );
		}
		return get_the_title();
	}

	public static function section_modifiers( $modifiers )
	{
		if ( !$modifiers ) return false;

		return implode( ' ', $modifiers );
	}

	public static function dynamic_col_class( $parent_count, $cols, $align = null )
	{
		if ( $parent_count === 1 && $align === 'centered' ) return 'col-' . $cols . '-centered';

		if ( $parent_count === 1 && $align !== 'centered' ) return 'col-' . $cols;

		if ( $cols === 'auto' ) return 'col-' . ( 12 / $parent_count );

		return 'col-' . $cols;
	}

	public static function posts( $args )
	{
		$loop = new \WP_Query( $args );
		return $loop->posts;
	}

	public static function excerpt( $post_id, $length = 50 )
	{
		$post = get_post( $post_id );

		//Create an excerpt from the content
		$excerpt = explode( ' ', $post->post_content, $length );
		if ( count( $excerpt ) >= $length ) {

			array_pop( $excerpt );
			$excerpt = implode( " ", $excerpt ) . '...';

		} else {

			$excerpt = implode( " ", $excerpt );

		}

		$excerpt = '<p>' . strip_tags( preg_replace( '`[[^]]*]`', '', $excerpt ) ) . '</p>';
		return $excerpt;

	}

	public static function staff_excerpt( $staff_bio, $length = 80 )
	{
		$post = $staff_bio;

		//Create an excerpt from the content
		$excerpt = explode( ' ', $post, $length );
		if ( count( $excerpt ) >= $length ) {

			array_pop( $excerpt );
			$excerpt = implode( " ", $excerpt ) . '...';

		} else {

			$excerpt = implode( " ", $excerpt );

		}

		$excerpt = '<p>' . strip_tags( preg_replace( '`[[^]]*]`', '', $excerpt ) ) . '</p>';
		return $excerpt;

	}

	public static function get_category_url( $slug )
	{
		return explode( '?', $_SERVER[ 'REQUEST_URI' ] )[ 0 ] . '?category=' . $slug;
	}

	public function breadcrumbs()
	{
		$output = '<section class="breadcrumbs"><ul>';
		if ( get_post_type() == 'post' ) {
			// $output .= '<li><a href="/resources">Resources</a></li>';
		}
		$output .= '<li><a href="resources-feed?category=' . get_the_category( $post->ID )[ 0 ]->slug . ' ">' . get_the_category( $post->ID )[ 0 ]->name . '</a></li>';
		$output .= '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';
		$output .= '</ul></section>';
		return $output;
	}

	public static function blog_posts()
	{
		global $page;

		$args = [
			'post_type'      => 'post',
			'posts_per_page' => 10,
			'paged'          => $page
		];

		$category = $_REQUEST[ 'category' ];

		if ( $category && $category !== 'all' ) {
			$args[ 'tax_query' ] = [
				[
					'taxonomy' => 'category',
					'field'    => 'slug',
					'terms'    => $category,
				]
			];
		}

		$loop = new \WP_Query( $args );
		return $loop->posts;
	}

	public function login_required()
	{
		if ( !get_field( 'login_required' ) || is_user_logged_in() ) return;

		if ( wp_redirect( home_url( 'sign-in' ) . '?message=login_to_view&referer=' . $_SERVER[ 'REQUEST_URI' ] ) ) {
			exit;
		}
	}

	public function cart_count()
	{
		$count = WC()->cart->cart_contents_count;

		return $count;

	}
}
