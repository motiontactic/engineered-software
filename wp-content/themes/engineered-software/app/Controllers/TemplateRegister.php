<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class TemplateRegister extends Controller
{
    public function register_user() {
        $errors = array();
        $new_user_id = 0;

        if( $_SERVER['REQUEST_METHOD'] !== 'POST' ) return ['user_id' => 0, 'errors' => []];

        $email = esc_sql($_REQUEST['email']);
        $username = $email;
        $password = $_POST['password'];
        $password_confirmation = $_POST['password_confirmation'];
        $redirect_url = $_POST['redirect_to'];

        // Check username is present and not already in use

        if ( strpos($username, ' ') !== false )
        {
            $errors['email'] = "Sorry, no spaces allowed in email";
        }
        if(empty($username))
        {
            $errors['email'] = "Please enter a email";
        } elseif( username_exists( $username ) )
        {
            $errors['email'] = "This email address is already in use";
        }

        // Check email address is present and valid
        if( !is_email( $email ) )
        {
            $errors['email'] = "Please enter a valid email";
        } elseif( email_exists( $email ) )
        {
            $errors['email'] = "This email address is already in use";
        }

        // Check password is valid
        if(preg_match("/.{6,}/", $password) === 0 )
        {
            $errors['password'] = "Password must be at least six characters";
        }

        // Check password confirmation_matches
        if(strcmp($password, $password_confirmation) !== 0)
        {
            $errors['password_confirmation'] = "Passwords do not match";
        }

        if(count($errors) === 0)
        {
            $new_user_id = wp_create_user( $username, $password, $email );

            wp_clear_auth_cookie();
            wp_set_current_user ( $new_user_id );
            wp_set_auth_cookie  ( $new_user_id );

            if ( wp_redirect( $redirect_url ) ) {
                exit;
            }
        }

        return ['user_id' => $new_user_id, 'errors' => $errors];
    }
}
