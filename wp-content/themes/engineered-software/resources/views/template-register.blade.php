{{--
  Template Name: Register
--}}

@extends('layouts.app')

@section('content')
    <section
            class="hero-section flex-row hero-short gray_overlay">
        <div class="content-container">
            <div class="hero-content col-8-centered">
                <h1 style="text-align: center;">Register</h1>
            </div>
        </div>
        @include('partials.bg', ['desktop' =>  '/wp-content/uploads/2019/07/ES_Ocean_2.jpg', 'mobile' => '/wp-content/uploads/2019/07/ES_Ocean_2.jpg'])
    </section>

    <section class="account register">
        <div class="content-container">
            <div class="col-6-centered">
                <div class="account-wrap">
                @if(is_user_logged_in())
                    <h5>You are currently logged in</h5>
                    @if(function_exists('wc_logout_url'))
                        <a href="{{ wc_logout_url('/sign-in') }}" class="button">Logout</a>
                    @else
                        <a href="{{ wp_logout_url('/sign-in') }}" class="button">Logout</a>
                    @endif
                @else
                    <h3>Register</h3>
                    <form name="registerform" id="registerform" action="" method="post" class="registerform">
                        <input type="text" name="email" id="email" value="" placeholder="Email" class=" @if($register_user['errors']['email']) error-state @endif ">
                        @if($register_user['errors']['email'])
                            <p class="input-error-message">{{ $register_user['errors']['email'] }}</p>
                        @endif
                        <input type="password" name="password" id="password" value="" size="20" placeholder="Password" class=" @if($register_user['errors']['password']) error-state @endif ">
                        @if($register_user['errors']['password'])
                            <p class="input-error-message">{{ $register_user['errors']['password'] }}</p>
                        @endif
                        <input type="password" name="password_confirmation" id="password_confirmation" value="" size="20" placeholder="Password Confirmation" class=" @if($register_user['errors']['password_confirmation']) error-state @endif ">
                        @if($register_user['errors']['password_confirmation'])
                            <p class="input-error-message">{{ $register_user['errors']['password_confirmation'] }}</p>
                        @endif
                        <div class="form-footer">
                            <div class="login-submit account-submit">
                                <input type="submit" name="wp-submit" id="wp-submit" class="button button-primary" value="Register">
                                <input type="hidden" name="redirect_to" value="{{ $_GET['referer'] ? $_GET['referer'] : home_url() }}">
                            </div>
                            <div class="account-alt-button">
                                <p>Current User?</p>
                                <a href="{{ home_url('sign-in') }}" class="button">Sign In</a>
                            </div>
                        </div>
                    </form>
                @endif
                </div>
            </div>
        </div>
    </section>
@endsection
