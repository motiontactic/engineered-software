<section class="product-form-two-column-section">
    <div class="content-container flex-row">
        <div class="product-list-cta fw-flex-col-6  fw-flex-col-tl-12">
            <div class="product-header">{!! get_sub_field('product_cta_header') !!}</div>
            <div class="product-list">
                @if(have_rows('product_list'))
                    @while (have_rows('product_list'))@php(the_row())
                    <div class="product-cta-card">
                        {!! get_sub_field('product_content') !!}
                    </div>
                    @endwhile
                @endif
            </div>

        </div>
        <div class="form-wrap form-green fw-flex-col-6  fw-flex-col-tl-12">
            <div class="form-header-content">{!! get_sub_field('form_header') !!}</div>
            @php(gravity_form(get_sub_field('form_select')['id'], true, false, false, '', false, 12 ))
        </div>
    </div>
</section>