@php($videoCount = count(get_sub_field('video_thumbnails')));

<section class="video_player_slider">
    <div class="content-container video-player-wrap">
        @if($videoCount === 1)
            <div class="video-player video-player--single">
                @if(have_rows('video_thumbnails'))
                    @while(have_rows('video_thumbnails'))@php(the_row())
                    <div class="video-slide"
                         data-video-url="{{get_sub_field('youtube_embed_url')}}">
                        <figure class="background thumbnail"
                                style="background-image:url({{get_sub_field('video_thumbnail')['url']}})"></figure>
                        @include('svg.icon-play')
                    </div>
                    @endwhile
                @endif
            </div>
        @else
            <div class="video-player">
                @if(have_rows('video_thumbnails'))
                    @while(have_rows('video_thumbnails'))@php(the_row())
                    <div class="video-slide"
                         data-video-url="{{get_sub_field('youtube_embed_url')}}">
                        <figure class="background thumbnail"
                                style="background-image:url({{get_sub_field('video_thumbnail')['url']}})"></figure>
                        @include('svg.icon-play')
                    </div>
                    @endwhile
                @endif
            </div>
            <div class="video-slider">
                @php($image_index = 0)
                @if(have_rows('video_thumbnails'))
                    @while(have_rows('video_thumbnails'))@php(the_row())
                    <div class="video-slide-thumb" data-video-index="{{$image_index}}">
                        <figure class="background thumbnail"
                                style="background-image:url({{get_sub_field('video_thumbnail')['url']}})"></figure>
                    </div>
                    @php($image_index ++)
                    @endwhile
                @endif
            </div>
        @endif
    </div>
</section>