@php($cols_count = count(get_sub_field('columns')))
<section
        class="full_width_image_with_content {{get_sub_field('color_overlay')}} {{ get_sub_field('section_text_align') }}">
    <div class="content-container content-wrap @if($cols_count > 1) has-cols @endif">
        <div class="@if($cols_count > 1) flex-row @endif">
            @if(get_sub_field('columns'))
                @foreach(get_sub_field('columns') as $col)
                    <div class="column-content @if($cols_count > 1) flex-@endif{{ App::dynamic_col_class($cols_count, $col['column_width'], $col['column_align']) }}">
                        {!! $col['content'] !!}
                    </div>
                @endforeach
            @endif
        </div>
        @if(get_sub_field('content_image_option') === true)
            <div class="content-image">
                <div class="content-image-wrap">
                    <img src="{{get_sub_field('content_image')['url']}}">
                </div>
            </div>
        @endif
    </div>
    @if(get_sub_field('background_image_option') === true)
        <figure class="background"
                style="background-image:url({{get_sub_field('background_image')['url']}})">
        </figure>
    @endif
</section>