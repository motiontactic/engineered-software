<section class="product-page-hero">
    <div class="content-container has-cols">
        <div class="product-hero-content col-7">
            {!! get_sub_field('product_hero_content') !!}
        </div>
    </div>
    <div class="showcase">
        <div class="showcase-wrap">
            <figure class="background product-hero-image-wrap"
                    style="background-image:url(<?php echo get_template_directory_uri() ?>/assets/images/computer-background.png);">

                <figure class="background product-image"
                        style="background-image:url({{get_sub_field('product_hero_image')['url']}})"></figure>
            </figure>
        </div>
    </div>
</section>