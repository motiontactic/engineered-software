<section
        class="hero-section flex-row {{ App::section_modifiers(get_sub_field('hero_height')) }} {{get_sub_field('color_overlay')}}">
    <div class="content-container">
        <div class="hero-content col-8-centered">
            {!! get_sub_field('hero_content') !!}
        </div>
    </div>
    @include('partials.bg', ['desktop' =>  get_sub_field('desktop_hero_image'), 'mobile' => get_sub_field('mobile_hero_image')])
</section>