<section class="testimonials-slider-section {{get_sub_field('color_overlay')}}">
    <figure class="background"
            style="background-image:url({{get_sub_field('section_background_image')['url']}})">
    </figure>
    <div class="content-container">
        <div class="testimonials-slider-wrap">
            <div class="testimonial-slider">
                @if(have_rows('testimonials'))
                    @while (have_rows('testimonials'))@php(the_row())
                    <div class="testimonial-slide">
                        <div class="testimonial-content col-10-centered">
                            {!! get_sub_field('testimonial_content')!!}
                        </div>
                        @if(get_sub_field('testimonial_logo_option') === true)
                            <div class="testimonial-logo">
                                <img src="{{get_sub_field('testimonial_logo')['url']}}">
                            </div>
                        @endif
                    </div>
                    @endwhile
                @endif
            </div>
        </div>
    </div>
</section>