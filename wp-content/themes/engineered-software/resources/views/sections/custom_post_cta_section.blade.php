<section class="custom-post-cta-section {{ App::section_modifiers(get_sub_field('section_modifiers')) }}">
    <div class="content-container">
        <div class="header-content col-10-centered">
            {!! get_sub_field('header_content') !!}
        </div>
        <div class="post-grid-wrap flex-row">
            @if(have_rows('post_call_out'))
                @while (have_rows('post_call_out'))@php(the_row())
                <div class="post-card flex-row">
                    <figure class="background"
                            style="background-image:url({{get_sub_field('post_image')['url']}})">
                    </figure>
                    <h4>{!! get_sub_field('post_title') !!}</h4>
                    <a class="post-url button--more"
                       href="{{get_sub_field('post_url')['url']}}">
                        {{get_sub_field('cta_content')}}
                    </a>
                </div>
                @endwhile
            @endif
        </div>
    </div>
</section>