<section class="logo-slider-section">
    <div class="logo-slider-wrap">
        <h5 class="text-center logo-slider-header">{!! get_sub_field('slider_title') !!}</h5>
        <div class="logo-slider">
            @if(have_rows('logos'))
                @while(have_rows('logos'))@php(the_row())
                <div class="logo-slide">
                    @if(get_sub_field('logo_url'))
                        <a href="{{get_sub_field('logo_url')['url']}}" target="_blank">
                            <img src="{{get_sub_field('logo')['url']}}">
                        </a>
                    @else
                        <img src="{{get_sub_field('logo')['url']}}">
                    @endif
                </div>
                @endwhile
            @endif
        </div>
    </div>
</section>