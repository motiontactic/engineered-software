<section class="full-width-two-column">
    <div class="content-wrap flex-row">
        @if(get_sub_field('columns'))
            @foreach(get_sub_field('columns') as $col)
                @if($col['column_type'] === 'content')
                    @if($col['latest_news_option'] === true)
                        <div class="col-width column-content {{$col['background_color']}}">
                            @php($post = App::posts(['post_type'=>'post', 'posts_per_page' => 1, 'category_name' => 'esi_in_the_news'])[0])
                            <div class="latest-news-header">Latest News</div>
                            <h4>{!! get_the_title($post->ID) !!}</h4>
                            <div class="news-excerpt">
                                {!! App::excerpt($post->ID) !!}
                            </div>
                            <div>
                                <a href="{{ get_the_permalink($post->ID) }}"
                                   class="button button--cta">Read More</a>
                            </div>
                        </div>
                    @else
                        <div class="col-width column-content {{$col['background_color']}}">
                            {!! $col['content'] !!}
                        </div>
                    @endif
                @endif
                @if($col['column_type'] === 'image')
                    <div class="image-wrap col-width">
                        <figure class="background" style="background-image:url({{ $col['image']['url']}})"></figure>
                    </div>
                @endif
            @endforeach
        @endif
    </div>
</section>