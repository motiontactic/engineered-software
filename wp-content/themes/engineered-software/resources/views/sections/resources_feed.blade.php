@php($blog_posts = App::blog_posts())
<?php
$current_page = (int)$_GET[ 'paged' ];
if ( !$current_page ) $current_page = 1;

$args = [
	'post_type'      => 'post',
	'posts_per_page' => 10,
	'paged'          => $current_page
];

$category = $_REQUEST[ 'category' ];

if ( $category && $category !== 'all' ) {
	$args[ 'tax_query' ] = [
		[
			'taxonomy' => 'category',
			'field'    => 'slug',
			'terms'    => $category,
		]
	];
}

$loop = new \WP_Query( $args );
$max_pages = (int)$loop->max_num_pages;
$blog_posts = $loop->posts;
?>
<section class="blog-hero gray_overlay">
    <div class="content-container blog-title-wrap">
        <h2>{!! get_the_title() !!}</h2>
    </div>
    <figure class="background"
            style="background-image:url({{get_sub_field('hero_image')['url']}})">
    </figure>
</section>

<section class="content-container feed-wrap">
    {{--<div class="breadcrumb-nav"><a href="/resources">< Back to Resources</a></div>--}}
    <div class="resources-feed-section col-9">
        @if($blog_posts)
            @foreach($blog_posts as $post)
                <div class="post-wrap">
                    <div class="post-header">
                        {!! get_the_title($post->ID) !!}
                    </div>
                    <time class="updated"
                          datetime="{{ get_post_time('c', true) }}">{{ get_the_date('', $post->ID) }}</time>
                    @foreach (get_the_category($post->ID) as $cat)
                        @if($cat->name != 'Uncategorized' )
                            <div class="cat-name">{!!$cat->name!!}</div>
                        @endif
                    @endforeach
                    @if(get_field('author_name', $post->ID))
                        <div class="article-author">
                            By {!! get_field('author_name', $post->ID) !!}, {!! get_field('author_title', $post->ID) !!}
                        </div>
                    @endif
                    <div class="post-excerpt">
                        {!! App::excerpt($post->ID) !!}
                    </div>
                    <div class="read-more-button flex-row">
                        <a href="{{get_the_permalink($post->ID)}}" class="button button--cta">Read
                            More</a>
                    </div>
                </div>
            @endforeach
        @endif
        @if($max_pages > 1)
            <div class="blog-feed-nav flex-row">
                @include('partials.pagination', ['current_page' => $current_page, 'max_pages' => $max_pages])
            </div>
        @endif
    </div>
    @include('partials.categories_sidebar')
</section>