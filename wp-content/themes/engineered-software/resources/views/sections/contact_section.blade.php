<section class="contact-section">
    <div class="content-container has-cols">
        <div class="contact-info col-6">
            <h5>Corporate Headquarters</h5>
            <div class="contact-address">{!! get_field('address', 'OPTIONS') !!}</div>
            <div class="contact-phone">
                <span class="contact-heading">Telephone:</span>
                <a href="tel:{{get_field('usa_phone_number', 'OPTIONS')}}" class="phone-usa">{!! get_field('usa_phone_number', 'OPTIONS') !!}</a>
            </div>
            <div class="contact-fax">
                <span class="contact-heading">Fax:</span>
                {!! get_field('usa_fax_number', 'OPTIONS') !!}
            </div>
            <div class="contact-email">
                <span class="contact-heading">Email:</span>
                {!! get_field('email', 'OPTIONS') !!}
            </div>
            <div class="contact-social">
                @if(have_rows('social_links', 'OPTIONS'))
                    @while (have_rows('social_links', 'OPTIONS'))@php(the_row())
                    <div class="social-link">
                        <a href="{{ get_sub_field('social_link', 'OPTIONS')}}" target="_blank">
                            {!! get_sub_field('social_icon', 'OPTIONS') !!}
                        </a>
                    </div>
                    @endwhile
                @endif
            </div>
        </div>
        <div class="contact-form col-6">
            <div class="form-header-content">{!! get_sub_field('form_header_content') !!}</div>
            <div class="form">
                @php(gravity_form(get_sub_field('form_select')['id'], true, false, false, '', false, 12 ))
            </div>
        </div>
    </div>
</section>