<section class="form-section">
    <div class="content-container flex-row">
        <div class="form-image-wrap fw-flex-col-6 fw-flex-col-tl-12">
            <figure class="background"
                    style="background-image:url({{get_sub_field('form_section_image')['url']}})">
            </figure>
        </div>
        <div class="form-wrap fw-flex-col-6  fw-flex-col-tl-12">
            <div class="form-header-content">{!! get_sub_field('form_header_content') !!}</div>
            <div class="form">
                @php(gravity_form(get_sub_field('form_select')['id'], true, false, false, '', false, 12 ))
            </div>
        </div>
    </div>
</section>