@php($cols_count = count(get_sub_field('columns')))
<section class="content-section {{ App::section_modifiers(get_sub_field('section_modifiers')) }}">
    <div class="content-container @if($cols_count > 1) has-cols @endif">
        <div class="@if($cols_count > 1) flex-row content-cols @endif">
            @if(get_sub_field('columns'))
                @foreach(get_sub_field('columns') as $col)
                    <div class="column-content @if($cols_count > 1) flex-@endif{{ App::dynamic_col_class($cols_count, $col['column_width'], $col['column_align']) }}
                    @if($col['icon_image']) icon-image @endif"
                         @if($col['anchor_id_option']) id="{{ $col['anchor_id'] }}"@endif>
                        {!! $col['content'] !!}
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</section>