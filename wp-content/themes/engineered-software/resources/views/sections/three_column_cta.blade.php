<section class="three_column_cta_section flex-row">
    <a href="{{get_sub_field('column_one_url')}}" class="column_one column-flex fw-flex-col-4 fw-flex-col-tl-12">
        {!! get_sub_field('column_one_content') !!}
    </a>
    <a href="{{get_sub_field('column_two_url')}}" class="column_two column-flex fw-flex-col-4 fw-flex-col-tl-12">
        {!! get_sub_field('column_two_content') !!}
    </a>
    <a href="{{get_sub_field('column_three_url')}}" class="column_three column-flex fw-flex-col-4 fw-flex-col-tl-12">
        {!! get_sub_field('column_three_content') !!}
    </a>
</section>