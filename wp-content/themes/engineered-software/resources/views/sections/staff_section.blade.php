<section class="staff-section">
    <div class="content-container">
        <h4>Leadership Team</h4>
        @if(get_sub_field('staff_members'))
            @foreach(get_sub_field('staff_members') as $staff)
                <div class="staff-member-wrap">
                    <div class="staff-bio">
                        <h4>{!! $staff['staff_name_and_title'] !!}</h4>
                        <div class="bio-excerpt">
                            {!! App::staff_excerpt($staff['staff_bio']) !!}
                        </div>
                        @if(str_word_count($staff['staff_bio']) > 80 )
                            <div class="bio-full">
                                {!! $staff['staff_bio'] !!}
                            </div>
                            <span class="button--more show-more">Read More</span>
                            <span class="button--more show-less">Read Less</span>
                        @endif
                    </div>
                    <div class="staff-image">
                        <figure class="background"
                                style="background-image:url({{$staff['staff_photo']['url']}})">
                        </figure>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
</section>

