<section class="icon-list-content">
    <div class="content-container content-wrap has-cols">
        <div class="icon-main-content col-4">
            {!! get_sub_field('content_section') !!}
        </div>
        <div class="icon-list col-8 flex-row">
            @if(have_rows('icon_list'))
                @while (have_rows('icon_list'))@php(the_row())
                <a href="{{get_sub_field('icon_link')['url']}}" class="icon-card">
                    <div class="icon-img">
                        <img src="{{get_sub_field('icon')['url']}}">
                    </div>
                    <div class="icon-info">
                        <h6>{!! get_sub_field('icon_title') !!}</h6>
                        <div>
                            {!! get_sub_field('icon_excerpt') !!}
                        </div>
                    </div>
                </a>
                @endwhile
            @endif
        </div>
    </div>
</section>