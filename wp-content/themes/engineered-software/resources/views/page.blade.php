@extends('layouts.app')

@section('content')
    @while(have_posts()) @php the_post() @endphp
    @if(have_rows('default_flexible_sections'))
        @php($flex_index = 0)
        @while(have_rows('default_flexible_sections')) @php(the_row())
        @includeIf('sections.' . get_row_layout(), ['flex_index' => $flex_index])
        @php($flex_index ++)
        @endwhile
    @else
        <div class="content-container">
            @php(the_content())
        </div>
    @endif
    @endwhile
@endsection
