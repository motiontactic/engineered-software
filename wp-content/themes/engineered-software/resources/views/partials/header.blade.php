<header class="banner @if(get_field('header_style')) header-{{ get_field('header_style')  }}  @else header-solid @endif ">
    <div class="content-container nav-wrap flex-row">
        <div class="brand">
            @if(get_field('header_style') === 'transparent')
                <a href="{{ home_url('/') }}">
                    <img src="{{ get_field('header_logo_white', 'OPTIONS')['url'] }}"
                         alt="{{ get_field('header_logo_white', 'OPTIONS')['alt'] }}">
                </a>
            @else
                <a href="{{ home_url('/') }}">
                    <img src="{{ get_field('header_logo_color', 'OPTIONS')['url'] }}"
                         alt="{{ get_field('header_logo_color', 'OPTIONS')['alt'] }}">
                </a>
            @endif
        </div>
        <nav class="nav-primary flex-row">
            @if (has_nav_menu('primary_navigation'))
                {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
            @endif
            <div class="header-right flex-row">
                <a href="{{get_field('request_quote_url','OPTIONS')['url']}}"
                   class="menu-button button--cta">{!! get_field('request_quote_text', 'OPTIONS') !!}</a>
                {{--<div class="cart-icon flex-row">--}}
                {{--<a href="/cart/">@include('svg.icon-card')--}}
                {{--@if($cart_count)--}}
                {{--<span class="cart-contents-count">{{$cart_count}}</span>--}}
                {{--@endif--}}
                {{--</a>--}}
                {{--</div>--}}
            </div>
        </nav>
        <div class="menu-toggle nav-break">
            <span class="line"></span>
            <span class="line center"></span>
            <span class="line"></span>
        </div>
    </div>
    <div class="mobile-menu">
        <div class="mobile-nav">
            @if (has_nav_menu('mobile_navigation'))
                {!! wp_nav_menu(['theme_location' => 'mobile_navigation', 'menu_class' => 'nav']) !!}
            @endif
            <div class="header-right flex-row">
                <a href="{{get_field('request_quote_url')['url'], 'OPTIONS'}}" class="button button--cta"
                   target="_blank">{!! get_field('request_quote_text', 'OPTIONS') !!}</a>
                {{--<div class="cart-icon flex-row">--}}
                {{--<a href="/cart/">@include('svg.icon-card')</a>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
</header>
