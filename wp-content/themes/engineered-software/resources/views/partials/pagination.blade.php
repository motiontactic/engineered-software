@if($current_page != 1)
    <a href="?paged={{ $current_page - 1 }}" class="prev-arrow nav-arrow"
       data-paged="{{ $current_page - 1 }}">@include ('svg.angle-left')</a>
@endif
<p class="pagination">Page {{ $current_page }} of {{ $max_pages }}</p>
@if($current_page !== $max_pages)
    <a href="?paged={{ $current_page + 1 }}" class="next-arrow nav-arrow"
       data-paged="{{ $current_page + 1 }}">@include ('svg.angle-right')</a>
@endif