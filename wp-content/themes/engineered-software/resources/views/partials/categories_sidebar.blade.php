<div class="categories-sidebar col-offset-1 col-2">
    <h5>Categories</h5>
    @php
        $_REQUEST['category']
    @endphp
    @foreach(get_terms(['taxonomy' => 'category']) as $cat)
        @if($cat->name != 'Uncategorized' )
            <a href="resources-feed?category={{$cat->slug}}"
               class="filter-opt @if( $_REQUEST['category'] === $cat->slug) active @endif">{!! $cat->name !!}</a>
        @endif
    @endforeach
</div>