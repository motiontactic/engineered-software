<section class="blog-hero gray_overlay">
    <div class="content-container blog-title-wrap">
        <h2>{!! get_the_title() !!}</h2>
    </div>
    @if(!get_the_post_thumbnail_url())
        <figure class="background"
                style="background-image:url({{get_field('default_featured_image', 'OPTIONS')['url']}})">
        </figure>
    @else
        <figure class="background"
                style="background-image:url({{get_the_post_thumbnail_url()}})">
        </figure>
    @endif
</section>

<div class="content-container blog-wrap">
    <div class="breadcrumb-nav">@php echo(App::breadcrumbs()) @endphp</div>
    <div class="flex-col has-cols">
        <div class="article-meta">
            <time class="updated" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>
            @foreach (get_the_category(get_the_ID()) as $cat)
                @if($cat->name != 'Uncategorized' )
                    <div class="cat-name">{!!$cat->name!!}</div>
                @endif
            @endforeach
            @if(get_field('author_name', $post->ID))
                <div class="article-author">
                    By {!! get_field('author_name', $post->ID) !!}, {!! get_field('author_title', $post->ID) !!}
                </div>
            @endif
        </div>
        <div class="article-wrap col-9">
            <article @php post_class() @endphp>
                <div class="entry-content">
                    @php the_content() @endphp
                </div>
                <div class="next-previous-blog flex-row">
                    @php($next_post = get_previous_post())
                    @if (!empty( $next_post ))
                        <a class="button--prev" href="{{get_permalink( $next_post->ID )}}">Previous Article</a>
                    @endif
                    @php($next_post = get_next_post())
                    @if (!empty( $next_post ))
                        <a class="button--more" href="{{get_permalink( $next_post->ID )}}">Next Article</a>
                    @endif
                </div>

            </article>
        </div>
        @include('partials.categories_sidebar')
    </div>
</div>
