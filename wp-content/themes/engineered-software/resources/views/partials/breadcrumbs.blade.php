<div class="breadcrumb-nav">
    <section class="breadcrumbs">
        <ul>
            <li><a href="/">Home</a></li>
            @if(get_post_type() === 'post')
                <li><a href="/resources">Resources</a></li>
        @endif
        <!-- Category-->
            <li><a href=""></a></li>
            <!-- If page is single blog post, blog title-->
            <li><a></a></li>
        </ul>
    </section>
</div>