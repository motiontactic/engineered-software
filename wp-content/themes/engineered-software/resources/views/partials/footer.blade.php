<footer class="footer-section">
    <div class="content-container">
        <!-- FOOTER COLUMNS -->
        <div class="flex-row footer-col-wrap">
            <div class="footer-column-flex contact-col flex-col-3 flex-col-tl-4 flex-col-t-6 flex-col-ml-12">
                <img src="{{get_field('footer_logo', 'OPTIONS')['url']}}"
                     alt="{{get_field('footer_logo', 'OPTIONS')['alt']}}">
                <div>
                    {!! get_field('address', 'OPTIONS') !!}
                </div>
                <div>
                    <div class="country-title">US & Canada</div>
                    <a href="tel:{{get_field('usa_phone_number', 'OPTIONS')}}" class="phone-usa">{!! get_field('usa_phone_number', 'OPTIONS') !!}</a>
                </div>
                <div>
                    <div class="country-title">International</div>
                    <a href="tel:{{get_field('international_phone_number', 'OPTIONS')}}" class="phone-int">{!! get_field('international_phone_number', 'OPTIONS') !!}</a>
                </div>
                <div>
                    {!! get_field('email', 'OPTIONS') !!}
                </div>
            </div>
            <div class="footer-column-flex about-col flex-col-2 flex-col-tl-4 flex-col-t-6 flex-col-ml-12">
                @if (has_nav_menu('footer_column_two'))
                    {!! wp_nav_menu(['theme_location' => 'footer_column_two', 'menu_class' => 'nav']) !!}
                @endif
            </div>
            <div class="footer-column-flex products-col flex-col-2 flex-col-tl-4 flex-col-t-6 flex-col-ml-12">
                @if (has_nav_menu('footer_column_three'))
                    {!! wp_nav_menu(['theme_location' => 'footer_column_three', 'menu_class' => 'nav']) !!}
                @endif
            </div>
            <div class="footer-column-flex resources-col flex-col-2 flex-col-tl-4 flex-col-t-6 flex-col-ml-12">
                @if (has_nav_menu('footer_column_four'))
                    {!! wp_nav_menu(['theme_location' => 'footer_column_four', 'menu_class' => 'nav']) !!}
                @endif
            </div>
            <div class="footer-column-flex support-col flex-col-3 flex-col-tl-4 flex-col-t-6 flex-col-ml-12">
                @if (has_nav_menu('footer_column_five'))
                    {!! wp_nav_menu(['theme_location' => 'footer_column_five', 'menu_class' => 'nav']) !!}
                @endif
                <div class="footer-social-wrap">
                    <h6>Follow Us</h6>
                    <div class="footer-social">
                        @if(have_rows('social_links', 'OPTIONS'))
                            @while (have_rows('social_links', 'OPTIONS'))@php(the_row())
                            <div class="social-link">
                                <a href="{{ get_sub_field('social_url', 'OPTIONS')}}" target="_blank">
                                    {!! get_sub_field('social_icon', 'OPTIONS') !!}
                                </a>
                            </div>
                            @endwhile
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!--FOOTER LOGOS-->
        <div class="flex-row footer-logo-wrap">
            @if(have_rows('footer_logos_row', 'OPTIONS'))
                @while (have_rows('footer_logos_row', 'OPTIONS'))@php(the_row())
                <img src="{{ get_sub_field('secondary_logo', 'OPTIONS')}}">
                @endwhile
            @endif

        </div>
        <!--FOOTER COPYRIGHT AND LEGAL-->
        <div class="flex-row footer-bottom-wrap">
            <div class="copyright-info flex-col-6 flex-col-tl-12">
                &copy; 1982 - <?php echo date( "Y" ); ?> Engineered Software, Inc. All Rights Reserved
            </div>
            <div class="footer-legal flex-row flex-col-6 flex-col-tl-12">
                @if (has_nav_menu('footer_legal_navigation'))
                    {!! wp_nav_menu(['theme_location' => 'footer_legal_navigation', 'menu_class' => 'nav']) !!}
                @endif
            </div>
        </div>

    </div>
</footer>

{{--@if (has_nav_menu('primary_navigation'))--}}
{{--{!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}--}}
{{--@endif--}}
