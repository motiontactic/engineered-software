@extends('layouts.app')
@section('content')
    <section class="section-404">
        <div class="content-container">
            <div class="content-404">
                {!!  get_field('content_404', 'OPTIONS')!!}
            </div>
        </div>
        <figure class="background"
                style="background-image:url({{get_field('background_image_404', 'OPTIONS')['url']}})">
        </figure>
    </section>
@endsection
