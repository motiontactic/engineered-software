{{--
  Template Name: Sign In
--}}

@extends('layouts.app')

@section('content')
    <section
            class="hero-section flex-row hero-short gray_overlay">
        <div class="content-container">
            <div class="hero-content col-8-centered">
                <h1 style="text-align: center;">Sign In</h1>
            </div>
        </div>
        @include('partials.bg', ['desktop' =>  '/wp-content/uploads/2019/07/ES_Ocean_2.jpg', 'mobile' => '/wp-content/uploads/2019/07/ES_Ocean_2.jpg'])
    </section>

    <section class="account sign-in">
        <div class="content-container">
            <div class="col-6-centered">
                <div class="account-wrap">
                @if(is_user_logged_in())
                    <h5>You are currently logged in</h5>
                    @if(function_exists('wc_logout_url'))
                        <a href="{{ wc_logout_url('/sign-in') }}" class="button">Logout</a>
                    @else
                        <a href="{{ wp_logout_url('/sign-in') }}" class="button">Logout</a>
                    @endif
                @else
                    <h3>Sign In</h3>
                    @if($_GET['registered_user'])
                        <p class="login-notifications registration-notification">Thank you for registering, Login here:</p>
                    @endif
                    @if($_GET['message'] === 'login_to_view' )
                        <p class="login-notifications please-login-notification">To access this page please login here:</p>
                    @endif
                    <form name="loginform" id="loginform" action="{{ site_url() }}/wp-login.php" method="post" class="loginform @if($_GET['login'] === 'failed') error-state @endif ">
                        <p class="login-username">
                            <input type="text" name="log" id="user_login" class="input @if($_GET['login'] === 'failed') error-state @endif " value="{{ $_GET['registered_user'] }}" size="20" placeholder="Email">
                        </p>
                        <p class="login-password">
                            <input type="password" name="pwd" id="user_pass" class="input @if($_GET['login'] === 'failed') error-state @endif " value="" size="20" placeholder="Password">
                        </p>
                        @if($_GET['login'] === 'failed')
                            <p class="input-error-message">Your Email or Password did not match our records</p>
                        @endif
                        <div class="form-footer">
                            <div class="login-submit account-submit">
                                <input type="submit" name="wp-submit" id="wp-submit" class="button button-primary" value="Log In">
                                <input type="hidden" name="redirect_to" value="{{ $_GET['referer'] ? $_GET['referer'] : home_url() }}">
                                <input type="hidden" name="rememberme"  value="forever">
                            </div>
                            <div class="account-alt-button">
                                <p>New User?</p>
                                <a href="{{ home_url('register') }}{{ $_GET['referer'] ? '?referer=' . $_GET['referer'] : '' }}" class="button">Register</a>
                            </div>
                        </div>
                    </form>
                @endif
                </div>
            </div>
        </div>
    </section>
@endsection
