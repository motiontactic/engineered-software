import '../autoload/responsive-bgs';
import testimonialSlider from '../autoload/testimonials-slider';
import basicJS from '../autoload/basic';
import logoSlider from '../autoload/logo-slider';
import videoSlider from '../autoload/video-player-slider';
import menu from '../autoload/menu';
import subMenuDirection from '../autoload/sub-menu-direction';

export default {
    init() {
        $('.responsive').bgResponsiveImage();
        testimonialSlider.init();
        basicJS.init();
        menu.init();
        logoSlider.init();
        videoSlider.init();
        subMenuDirection.init();
    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
    },
};
