export default {

    init: function () {
        this.slider = $('.video-slider');
        this.slide = $('.video-slide');
        if (this.slide.length === 0) return;
        this.initSlider();
        this.bindEvents();
    },

    initSlider: function () {

        $('.video-player').slick({
            asNavFor: '.video-slider',
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            fade: true,
        });

        $('.video-slider').slick({
            asNavFor: '.video-player',
            dots: false,
            arrows: true,
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: false,
            adaptiveHeight: true,
            prevArrow: '<div class="prev-arrow"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-left"\n' +
                '     class="svg-inline--fa fa-arrow-left fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">\n' +
                '    <path fill="#3c296d"\n' +
                '          d="M257.5 445.1l-22.2 22.2c-9.4 9.4-24.6 9.4-33.9 0L7 273c-9.4-9.4-9.4-24.6 0-33.9L201.4 44.7c9.4-9.4 24.6-9.4 33.9 0l22.2 22.2c9.5 9.5 9.3 25-.4 34.3L136.6 216H424c13.3 0 24 10.7 24 24v32c0 13.3-10.7 24-24 24H136.6l120.5 114.8c9.8 9.3 10 24.8.4 34.3z"></path>\n' +
                '</svg></div>',
            nextArrow: '<div class="next-arrow"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="arrow-right"\n' +
                '     class="svg-inline--fa fa-arrow-right fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">\n' +
                '    <path fill="#3c296d"\n' +
                '          d="M190.5 66.9l22.2-22.2c9.4-9.4 24.6-9.4 33.9 0L441 239c9.4 9.4 9.4 24.6 0 33.9L246.6 467.3c-9.4 9.4-24.6 9.4-33.9 0l-22.2-22.2c-9.5-9.5-9.3-25 .4-34.3L311.4 296H24c-13.3 0-24-10.7-24-24v-32c0-13.3 10.7-24 24-24h287.4L190.9 101.2c-9.8-9.3-10-24.8-.4-34.3z"></path>\n' +
                '</svg></div>',
            responsive: [
                {
                    breakpoint: 1026,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                    },
                },
                {
                    breakpoint: 900,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    },
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    },
                },
            ],
        });
    },


    goToSlide: function ($$) {
        $('.video-player').slick('slickGoTo', $$.data('video-index'));
    },

    playVideo: function ($$) {
        let parent = $$.parent();
        let urlData = parent.data('video-url');
        console.log(urlData);
        $$.addClass('hidden');
        parent.find('.thumbnail').hide();
        parent.append(`<iframe src="${urlData}?autoplay=1" allow="autoplay"></iframe>`).fadeIn();
    },

    bindEvents: function () {
        let _this = this;

        $(document).on('click', '.video-slide-thumb', function () {
            _this.goToSlide($(this));
        });

        console.log('video play');

        $(document).on('click', '.play-video', function () {
            console.log('video play');
            _this.playVideo($(this));
        });

        $('.video-player').on('afterChange', function () {
            $('.video-player .video-slide .thumbnail').show();
            $('.video-player .video-slide .play-video').removeClass('hidden');
            $('.video-player .video-slide iframe').remove();
        });
    },
};