export default {

    init: function () {
        console.log('running');
        this.bindEvents();
    },

    setDirection: function ($$) {

        let remainingRightSpace = $(window).innerWidth() - $$.offset().left - $$.width(),
            subMenuWidth = $$.find('.sub-menu').width();

        $('.menu-item').removeClass('sub-menu-left');

        if (remainingRightSpace < subMenuWidth) {
            $$.addClass('sub-menu-left');
        }
    },

    bindEvents: function () {

        let _this = this;

        $('.nav-primary .menu-item.menu-item-has-children .menu-item.menu-item-has-children').hover(function () {
            console.log($(this));
            _this.setDirection($(this));
        })
    },

}