export default {

    staffBio: $('.staff-bio'),
    bioExcerpt: $('.bio-excerpt'),
    bioFull: $('.bio-full'),
    showMore: $('.show-more'),
    showLess: $('.show-less'),

    init: function () {
        this.bindEvents();
    },

    showFullBio: function ($$) {
        let parent = $$.parent();
        parent.find('.bio-excerpt').hide();
        parent.find('.show-more').hide();
        parent.find('.show-less').fadeIn();
        parent.find('.bio-full').fadeIn();
    },

    showLessBio: function ($$) {
        let parent = $$.parent();
        parent.find('.bio-full').hide();
        parent.find('.show-less').hide();
        parent.find('.bio-excerpt').fadeIn();
        parent.find('.show-more').fadeIn();
    },

    bindEvents: function () {

        let _this = this;

        this.showMore.click(function () {
            _this.showFullBio($(this));
        });

        this.showLess.click(function () {
            _this.showLessBio($(this));
        });

    },

}