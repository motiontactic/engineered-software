export default {

    menu: $('.menu, .mobile-menu'),
    toggle: $('.menu-toggle'),
    mobile_menu_item: $('.mobile-nav .menu-item a'),
    open: false,

    init: function () {

        this.bindEvents();
    },

    toggleMenu: function () {

        this.toggle.toggleClass('open');
        this.menu.toggleClass('open');
    },

    openSubNav: function ($$, e) {

        let parent = $$.parent('li'),
            sub_menu = parent.children('.sub-menu');

        if (parent.hasClass('menu-item-has-children')) {
            e.preventDefault();
            sub_menu.slideToggle();
        }
    },

    bindEvents: function () {

        let _this = this;

        this.toggle.click(function () {

            _this.toggleMenu();
        });

        this.mobile_menu_item.click(function (e) {
            _this.openSubNav($(this), e);
        });
    },

}