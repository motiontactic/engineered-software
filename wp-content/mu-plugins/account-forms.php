<?php
/**
 * Plugin Name: Account Forms
 * Plugin URI:
 * Description: Adds functionality for the website
 * Version: 1.0
 * Author:
 * Author URI:
 * License: GPL2
 */

defined('ABSPATH') or die("No script kiddies please!");

include_once 'account-forms/_functions.php';
