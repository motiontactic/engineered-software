<?php
function button($atts, $content = null)
{

    $a = shortcode_atts(array(
        'href' => '',
        'class' => '',
        'target' => '',
    ), $atts);

    return sprintf('<a href="%s" class="%s" target="%s">%s</a>', $a['href'], $a['class'], $a['target'], $content);
}

add_shortcode('button', 'button');