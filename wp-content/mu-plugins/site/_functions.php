<?php

function bool_output($output, $bool)
{
    if ($bool) return $output;
}

function bm_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

add_filter('upload_mimes', 'bm_mime_types');